<h1>K0T3K Witness Proposal</h1>
<p>
Welcome to my witness proposal.  Read all page to understand the project.
</p>
<h2>Introduction</h2>
<p>
I started to support cryptocurrencies back in early 2014. As in everything I do, I started by reading available documentation and forums. Quiet fast I started acquiring mining technology and got myself interested in mining, nodes, wallets and the famous blockchain.
A few months ago I decided to go green with the help of Rpi and similars...
</p>
<h2>Why house it myself instead to delegate it to Service Providers?</h2>
<p>
For me the reason is obvious but I assume some of you would wonder why!
<ul>
	<li>The first reason is the challenge! I like this stuff and I do what I can to understand it and support it!</li>
	<li>The second reason is the support! The best way to ensure it's decentralization, well it's runnibg as many private nodes as possible!</li>
	<li>Finally but not the less, the fact that I easily made the setup to go green, except for the brandwidth, everything is free! This teached me a lot and I'm sure it will serve me in the near future..</li>
</ul>
</p>
<h2>My hardware</h2>
<p>
I have a cluster of 10 Rpi2 managed throught a central unit: cubox-i pro. Each unit as 512mb RAM and the cubox-i as 2024mb RAM. They all run with 1 2-3A 5V adapter which drain a total of 1KW~.
They all write to an external sdd of 180GB wich is binded to a 8T HDD. Power consuption: 2x 50W. 
I also have mining hardware working on each unit of Rpis, it includes 2 Antminer S5, 10 USB S2, and 4 U3, roughly 2.5KW.  
</p>
<h2>The software</h2>
<p>
I currently run the following nodes in my farm:
<ul>
	<li>Bitcoin</li>
	<li>BitShares</li>
	<li>Ethereum</li>
	<li>Monero</li>
</ul>
I will add the following nodes very shortly! 
<ul>
	<li>Darkcoin/Dash</li>
</ul>
</p>
<h2>My Green Setup</h2>
<p>
I use a combination of the following sources: heat, sun, water and wind! I have 2 big solar panels on the roof, 8 cells that I placed in our old parabolic antenna of 1,20m width. The center of the antenna as been cutted and a mini-solar hoven has been installed; it points down and boils the water circuit to generate electricity. Finally I use water from the rain wich is then collected and injected in the boiling circuit, the excess is driven to a cold circuit where with the help of gravity it generates a few more watts... It all goes to my 8-pack of acid batteries coupled to a modulator which allow me to pump 3.5-4KW~. I have an uptime till now of 99,99% and will to everything I can to never go bellow 99%!
</p>
<h2>Why I ask for your votes?</h2>
<p>
The goal here, as I hope to have made it clear, enough is not to become rich, but it is to support decentralization! Nevertheless there are some investments that require some sort of return of investment! There is also the cost of the brandwidth. Even if I love doing it, supporting decentralization, it costs me time, for all those reasons I need your support!

Nevertheless I'm the first person to say that you dont get anything for anything, so I'm willing to spend some time to build up a website with all the tutorials to go green. Everything is already available in the internet but if I can cutting the time spent searching for a single one of you, I'm already happy! This will be done in the near future, as I'm currently overcharged with ideas and work but it will be done, with your help! ;)

P.S.: I'm neither an engineer nor an IT guy!, just learned it all myself and if I can you can too!!! What thrives Humanity is not money neither power, it's curiosity!

Thx for reading!!!

VOTE: k0t3k - Witness Proposal! 
</p>

<a href="https://bitshares.openledger.info?r=k0t3k"><h3>Join BitShares</h3></a>

